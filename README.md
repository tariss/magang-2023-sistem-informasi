# Magang 2023

## Name
Nama    : Taris Styaningrum                                     
NIM     : 2000016122                                                       
Posisi  : System Analyst                                                
Lokasi  : PT. Woolu Aksara Maya                

## Description
Melakukan analisis proses bisnis, dokumentasi, Research and Development.

## Support
Pembimbing Lapangan : Irfan Adam, S.T

## Roadmap
- [X] [1 | Analisis Data Requirement](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/1#tab-issues)
- [X] [2 | Flow Proses Bisnis platform mitra LKPP](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/2#tab-issues)
- [X] [3 | Rule dan regulasi](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/3#tab-issues)
- [X] [4 | Desain Produk dan Dashboard S-LIME](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/4#tab-issues)
- [X] [5 | Sistem Manajemen Bisnis SIPLAH](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/5#tab-issues)
- [X] [6 | Rancangan Arsitektur IT](https://gitlab.com/tariss/magang-2023-sistem-informasi/-/milestones/6#tab-issues)

## Project status
On Progress
